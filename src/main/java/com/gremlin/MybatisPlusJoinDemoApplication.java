package com.gremlin;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@Slf4j
@SpringBootApplication
public class MybatisPlusJoinDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybatisPlusJoinDemoApplication.class, args);
    }

    @Bean
    public ApplicationRunner applicationRunner(){
        return args -> log.info("*********************** 启动成功！***********************");
    }
}
