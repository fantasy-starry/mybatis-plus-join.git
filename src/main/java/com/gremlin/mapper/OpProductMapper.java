package com.gremlin.mapper;

import com.github.yulichang.base.MPJBaseMapper;
import com.gremlin.entity.OpProduct;
import org.apache.ibatis.annotations.Mapper;

/**
 * @className: OpProductMapper
 * @author: gremlin
 * @version: 1.0.0
 * @description:
 * @date: 2022/11/28 10:47
 */
@Mapper
public interface OpProductMapper extends MPJBaseMapper<OpProduct> {
}