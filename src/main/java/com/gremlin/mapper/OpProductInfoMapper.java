package com.gremlin.mapper;

import com.github.yulichang.base.MPJBaseMapper;
import com.gremlin.entity.OpProductInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @className: OpProductInfoMapper
 * @author: gremlin
 * @version: 1.0.0
 * @description:
 * @date: 2022/11/28 10:47
 */
@Mapper
public interface OpProductInfoMapper extends MPJBaseMapper<OpProductInfo> {
}
