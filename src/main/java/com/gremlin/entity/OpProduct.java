package com.gremlin.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("product")
public class OpProduct implements Serializable {

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;
    private String type;

}