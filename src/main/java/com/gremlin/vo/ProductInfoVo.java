package com.gremlin.vo;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @className: ProductInfoVo
 * @author: gremlin
 * @version: 1.0.0
 * @description:
 * @date: 2022/11/28 10:46
 */
@Data
@Accessors(chain = true)
public class ProductInfoVo {

    private String id;

    private String type;

    private String name;

    private String price;
}
