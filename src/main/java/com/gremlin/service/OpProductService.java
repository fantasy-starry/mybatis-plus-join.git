package com.gremlin.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.yulichang.base.MPJBaseService;
import com.gremlin.entity.OpProduct;
import com.gremlin.vo.ProductInfoVo;

import java.util.List;

/**
 * @className: OpProductService
 * @author: gremlin
 * @version: 1.0.0
 * @description:
 * @date: 2022/11/28 10:51
 */
public interface OpProductService extends MPJBaseService<OpProduct> {

    /**
     * lambda表达式查询
     */
    List<ProductInfoVo> queryAllProductLambda();

    /**
     * 普通QueryWrapper
     */
    List<ProductInfoVo> queryAllProduct();

    /**
     * 分页
     */
    IPage<ProductInfoVo> queryPageProduct(Integer pageNo, Integer pageSize);
}
